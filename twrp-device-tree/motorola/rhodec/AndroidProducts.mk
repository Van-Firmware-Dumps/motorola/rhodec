#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_rhodec.mk

COMMON_LUNCH_CHOICES := \
    omni_rhodec-user \
    omni_rhodec-userdebug \
    omni_rhodec-eng
