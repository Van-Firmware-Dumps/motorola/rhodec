#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_rhodec-user
add_lunch_combo omni_rhodec-userdebug
add_lunch_combo omni_rhodec-eng
