#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_rhodec.mk

COMMON_LUNCH_CHOICES := \
    lineage_rhodec-user \
    lineage_rhodec-userdebug \
    lineage_rhodec-eng
