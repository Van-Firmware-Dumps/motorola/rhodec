#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from rhodec device
$(call inherit-product, device/motorola/rhodec/device.mk)

PRODUCT_DEVICE := rhodec
PRODUCT_NAME := lineage_rhodec
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g62 5G
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="rhodec_g-user 13 T1SSS33M.1-119-8-12 fa11a release-keys"

BUILD_FINGERPRINT := motorola/rhodec_g/rhodec:13/T1SSS33M.1-119-8-12/fa11a:user/release-keys
